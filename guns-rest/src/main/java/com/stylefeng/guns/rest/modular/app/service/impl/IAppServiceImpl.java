package com.stylefeng.guns.rest.modular.app.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.rest.common.persistence.dao.DetailMapper;
import com.stylefeng.guns.rest.common.persistence.model.Detail;
import com.stylefeng.guns.rest.modular.app.service.IAppService;
import org.springframework.stereotype.Service;

/**
 * @Auther: liaoshiquan
 * @Date: 2018/6/18 14:23
 * @Description:
 */
@Service
public class IAppServiceImpl extends ServiceImpl<DetailMapper, Detail> implements IAppService {

}
