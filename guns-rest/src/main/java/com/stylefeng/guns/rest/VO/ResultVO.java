package com.stylefeng.guns.rest.VO;

import lombok.Data;

/**
 * @Auther: liaoshiquan
 * @Date: 10/04/2018 11:41
 * @Description: http请求返回的最外层对象
 * @Data 注解方便我们不用去复习get, set方法
 */
@Data
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultVO<T> {
    /*状态码*/
    private Integer code;

    /*提示信息*/
    private String message;

    /*具体内容*/
    private T data;
}
