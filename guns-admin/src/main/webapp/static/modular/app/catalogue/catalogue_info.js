/**
 * 初始化app首页配置详情对话框
 */
var CatalogueInfoDlg = {
    catalogueInfoData : {}
};

/**
 * 清除数据
 */
CatalogueInfoDlg.clearData = function() {
    this.catalogueInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CatalogueInfoDlg.set = function(key, val) {
    this.catalogueInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
CatalogueInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
CatalogueInfoDlg.close = function() {
    parent.layer.close(window.parent.Catalogue.layerIndex);
}

/**
 * 收集数据
 */
CatalogueInfoDlg.collectData = function() {
    this
    .set('id')
    .set('catalogueName')
    .set('imgUrl')
    .set('url')
    .set('urlType')
    .set('catalogueIndex')
    .set('dataType');
}

/**
 * 提交添加
 */
CatalogueInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/catalogue/add", function(data){
        Feng.success("添加成功!");
        window.parent.Catalogue.table.refresh();
        CatalogueInfoDlg.close();
    },function(data){
        Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.catalogueInfoData);
    ajax.start();
}

/**
 * 提交修改
 */
CatalogueInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    //提交信息
    var ajax = new $ax(Feng.ctxPath + "/catalogue/update", function(data){
        Feng.success("修改成功!");
        window.parent.Catalogue.table.refresh();
        CatalogueInfoDlg.close();
    },function(data){
        Feng.error("修改失败!" + data.responseJSON.message + "!");
    });
    ajax.set(this.catalogueInfoData);
    ajax.start();
}

$(function() {

});
