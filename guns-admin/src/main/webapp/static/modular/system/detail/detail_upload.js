/**
 * 初始化taobao详情对话框
 */
var DetailInfoDlg = {
    detailInfoData: {}
};

DetailInfoDlg.init = function () {
    var uploader = WebUploader.creat({
        auto: true,// 选完文件后，自动上传。
        swf: '../static/Uploader.swf',// swf文件路径
        server: '/upload',// 文件接收服务端。
        pick: '#picker',// 选择文件的按钮。可选。
    });

    // 当有文件被添加进队列的时候
    uploader.on('fileQueued', function (file) {
        $("#picker").hide();//隐藏上传框
        $("#thelist").append('<div id="' + file.id + '" class="item">' +
            '<h4 class="info">' + file.name + '</h4>' +
            '<p class="state"></p>' +
            '</div>');
    });
    // 文件上传过程中创建进度条实时显示。
    uploader.on('uploadProgress', function (file, percentage) {
        var $li = $('#' + file.id),
            $percent = $li.find('.progress .progress-bar');

        // 避免重复创建
        if (!$percent.length) {
            $percent = $('<div class="progress progress-striped active">' +
                '<div class="progress-bar" role="progressbar" style="width: 0%"></div>' +
                '</div><button id="cancel" onclick="cancel()" type="button" style="float:left;margin-left:10px;" class="btn btn-danger btn-sm">cancel</button>').appendTo($li).find('.progress-bar');
        }
        //alert(percentage)
        $li.find('p.state').text('Uploading');
        $percent.css('width', percentage * 100 + '%');
    });

    //上传成功后
    uploader.on('uploadSuccess', function (file, response) {
        if (response) {
            $('#' + file.id).find('p.state').text('upload success');
        } else {
            $('#' + file.id).find('p.state').text('upload error');
        }
    });

    //不管成功或者失败，文件上传完成时触发。
    uploader.on('uploadComplete', function (file) {
        $('#' + file.id).find('.progress').fadeOut();
    });

}

/**
 * 关闭此对话框
 */
DetailInfoDlg.close = function () {
    parent.layer.close(window.parent.Detail.layerIndex);
}


$(function () {
    DetailInfoDlg.init();
});
