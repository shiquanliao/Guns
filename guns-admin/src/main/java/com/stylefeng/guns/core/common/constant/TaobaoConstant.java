package com.stylefeng.guns.core.common.constant;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Auther: liaoshiquan
 * @Date: 2018/6/14 17:28
 * @Description:
 */
public class TaobaoConstant {
    public static final List<String> taobaoExcelName = Arrays.asList(
            "商品id", "商品名称", "商品价格(单位：元)", "商品一级类目",
            "券后价", "优惠券面额", "平台类型", "店铺名称",
            "商品链接", "商品主图", "收入比率(%)", "开推时间",
            "优惠券开始时间", "优惠券结束时间", "优惠券总量", "优惠券剩余量",
            "优惠券id", "优惠券推广链接", "推广链接", "官方推荐",
            "商品详情页链接地址", "淘宝客链接","商品月销量"
    );

}
