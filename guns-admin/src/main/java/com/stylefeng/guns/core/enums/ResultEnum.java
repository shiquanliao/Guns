package com.stylefeng.guns.core.enums;

import lombok.Data;

/**
 * @Auther: liaoshiquan
 * @Date: 2018/6/14 14:09
 * @Description:
 */
public enum ResultEnum {
    FILE_SAVE_ERROR(-1,"数据入库失败"),
    FILE_EMPTY_ERROR(11,"文件不能为空"),
    FILE_TYPE_ERROR(12,"文件格式不对"),
    FILE_SIZE_EMPTY_ERROR(13,"此文件为空文件")
    ;

    private int code;
    private String message;

    ResultEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
