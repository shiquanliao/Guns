package com.stylefeng.guns.modular.system.dao;

import com.stylefeng.guns.modular.system.model.Detail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 淘宝推广详情表 Mapper 接口
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
public interface DetailMapper extends BaseMapper<Detail> {

}
