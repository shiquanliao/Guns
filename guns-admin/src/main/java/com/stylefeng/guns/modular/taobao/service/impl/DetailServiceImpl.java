package com.stylefeng.guns.modular.taobao.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.stylefeng.guns.core.common.constant.TaobaoConstant;
import com.stylefeng.guns.core.util.ExcelImportUtils;
import com.stylefeng.guns.modular.system.model.Detail;
import com.stylefeng.guns.modular.system.dao.DetailMapper;
import com.stylefeng.guns.modular.taobao.service.IDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.print.Book;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 淘宝推广详情表 服务实现类
 * </p>
 *
 * @author stylefeng123
 * @since 2018-06-13
 */
@Service
public class DetailServiceImpl extends ServiceImpl<DetailMapper, Detail> implements IDetailService {

    @Override
    public Boolean batchImport(String saveFilePath, String fileName, MultipartFile file) {
        File uploadDir = new File(saveFilePath);
        //创建一个目录 （它的路径名由当前 File 对象指定，包括任一必须的父路径。）
        if (!uploadDir.exists()) uploadDir.mkdirs();
        //新建一个文件
        File tempFile = new File(saveFilePath + fileName);
        //初始化输入流
        InputStream is = null;
        try {
            //将上传的文件写入新创建的文件中
            file.transferTo(tempFile);

            //根据新建的文件实例化输入流
            is = new FileInputStream(tempFile);

            //根据版本选择创建Workbook的方式
            Workbook wb = null;
            if (ExcelImportUtils.isExcel2007(fileName)) {
//                wb = new XSSFWorkbook(is);
            } else {
                wb = new HSSFWorkbook(is);
            }
            //根据excel里面的内容读取知识库信息
            String userName = null;
            return readExcelValue(wb, userName, tempFile);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    is = null;
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    private boolean readExcelValue(Workbook wb, String userName, File tempFile) {
        Sheet firstSheet = wb.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();
        List<Detail> taobaoDataList = new ArrayList<>();
        int indeRow = 0; //标识当前行数

        List<Integer> indexList = new ArrayList<>();
        while (iterator.hasNext()) { //行迭代
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            Detail taobao = new Detail();
            indeRow++;

            while (cellIterator.hasNext()) { //列迭代
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();
                if (indeRow == 1) { //表示是第一行
                    String firstRowName = nextCell.getStringCellValue(); //第一行第几列的名字
                    int index = TaobaoConstant.taobaoExcelName.indexOf(firstRowName);//匹配静态list中的位置
                    indexList.add(index); //添加当前列应该插入数据库的哪个字段
                } else {
                    //把列中的数据装载到Detail模型对应的位置
                    setValue(nextCell, columnIndex, indexList, taobao);
                }
            }
            if (indeRow != 1) {
                taobaoDataList.add(taobao);
                //插入数据库
                Integer result = baseMapper.insert(taobao);
                if (result == 1) System.out.println("第" + indeRow + "行数据添加成功 ");
            }

        }

        return true;
    }

    /**
     * @param cell        列的单元格
     * @param columnIndex 当前是那一列,这样就能取出对应indexList中的数值(对应于数据库中的字段)
     * @param indexList   数据库中的字段
     */
    private void setValue(Cell cell, int columnIndex, List<Integer> indexList, Detail taobaoData) {
        // 对应表中的字段,比如 当前Excel是第2列,但是对应的数据库中是要插入 商品ID(第5列)
        Integer columnIndexOfTable = indexList.get(columnIndex);
        switch (columnIndexOfTable) {
            case 0: //商品id
                taobaoData.setProductId((String) getCellValue(cell));
                break;
            case 1: //商品名称
                taobaoData.setProductName((String) getCellValue(cell));
                break;
            case 2: //商品价格
                taobaoData.setProductPrice(Float.valueOf((String) Objects.requireNonNull(getCellValue(cell))));
                break;
            case 3: //商品一级类目
                taobaoData.setFirstType((String) getCellValue(cell));
                break;
            case 4: //券后价
                taobaoData.setTicketdPrice((float) getCellValue(cell));
                break;
            case 5: //优惠券面额
                taobaoData.setTicketDenomination((String) getCellValue(cell));
                break;
            case 6: //平台类型
                taobaoData.setPlatformType((String) getCellValue(cell));
                break;
            case 7: //店铺名称
                taobaoData.setNameOfShop((String) getCellValue(cell));
                break;
            case 8: //商品链接
                taobaoData.setPriductUrl((String) getCellValue(cell));
                break;
            case 9: //商品主图
                taobaoData.setPriductImage((String) getCellValue(cell));
                break;
            case 10: //收入比率
                taobaoData.setIncomeRatio(Float.valueOf((String) Objects.requireNonNull(getCellValue(cell))));
                break;
            case 11: //开推时间
                taobaoData.setActivityTimeStart(stringToDate((String) getCellValue(cell)));
                break;
            case 12: //优惠券开始时间
                taobaoData.setTicketTimeStart(stringToDate((String) getCellValue(cell)));
                break;
            case 13: //优惠券结束时间
                taobaoData.setTicketTimeEnd(stringToDate((String) getCellValue(cell)));
                break;
            case 14: //优惠券总量
                taobaoData.setTicketAmount(Integer.valueOf((String) Objects.requireNonNull(getCellValue(cell))));
                break;
            case 15: //优惠券剩余量
                taobaoData.setTicketResidue(Integer.valueOf((String) Objects.requireNonNull(getCellValue(cell))));
                break;
            case 16: //优惠券id
                taobaoData.setTicketId((String) getCellValue(cell));
                break;
            case 17: //优惠券推广链接
                taobaoData.setTicketGeneralizeUrl((String) getCellValue(cell));
                break;
            case 18: //推广链接
                taobaoData.setReferralLinks((String) getCellValue(cell));
                break;
            case 19: //官方推荐
                taobaoData.setRecommend((String) getCellValue(cell));
                break;
            case 20: //商品详情页
                taobaoData.setProductDetailPage((String) getCellValue(cell));
                break;
            case 21: //淘宝客链接
                taobaoData.setTaobaoGuestUrl((String) getCellValue(cell));
                break;
            case 22: //月销量
                taobaoData.setProductMonthlySales(Integer.valueOf((String) Objects.requireNonNull(getCellValue(cell))));
                break;
        }
    }

    private Date stringToDate(String cellValue) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null; //初始化date
        try {
            date = sdf.parse(cellValue); //Mon Jan 14 00:00:00 CST 2013
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private Object getCellValue(Cell cell) {
        switch (cell.getCellTypeEnum()) {
            case STRING:
                return cell.getStringCellValue();

            case BOOLEAN:
                return cell.getBooleanCellValue();

            case _NONE:
                break;
            case NUMERIC:
                return cell.getNumericCellValue();
            case FORMULA:
                break;
            case BLANK:
                break;
            case ERROR:
                break;
        }

        return null;
    }

}
//
//    CELL_TYPE_NUMERIC 数值型 0
//        CELL_TYPE_STRING 字符串型 1
//        CELL_TYPE_FORMULA 公式型 2
//        CELL_TYPE_BLANK 空值 3
//        CELL_TYPE_BOOLEAN 布尔型 4
//        CELL_TYPE_ERROR 错误 5
