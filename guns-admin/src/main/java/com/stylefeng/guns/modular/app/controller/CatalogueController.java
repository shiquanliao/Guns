package com.stylefeng.guns.modular.app.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import org.springframework.web.bind.annotation.RequestParam;
import com.stylefeng.guns.modular.system.model.Catalogue;
import com.stylefeng.guns.modular.app.service.ICatalogueService;

/**
 * app首页配置控制器
 *
 * @author fengshuonan
 * @Date 2018-06-19 16:25:03
 */
@Controller
@RequestMapping("/catalogue")
public class CatalogueController extends BaseController {

    private String PREFIX = "/app/catalogue/";

    @Autowired
    private ICatalogueService catalogueService;

    /**
     * 跳转到app首页配置首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "catalogue.html";
    }

    /**
     * 跳转到添加app首页配置
     */
    @RequestMapping("/catalogue_add")
    public String catalogueAdd() {
        return PREFIX + "catalogue_add.html";
    }

    /**
     * 跳转到修改app首页配置
     */
    @RequestMapping("/catalogue_update/{catalogueId}")
    public String catalogueUpdate(@PathVariable Integer catalogueId, Model model) {
        Catalogue catalogue = catalogueService.selectById(catalogueId);
        model.addAttribute("item",catalogue);
        LogObjectHolder.me().set(catalogue);
        return PREFIX + "catalogue_edit.html";
    }

    /**
     * 获取app首页配置列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return catalogueService.selectList(null);
    }

    /**
     * 新增app首页配置
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Catalogue catalogue) {
        catalogueService.insert(catalogue);
        return SUCCESS_TIP;
    }

    /**
     * 删除app首页配置
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer catalogueId) {
        catalogueService.deleteById(catalogueId);
        return SUCCESS_TIP;
    }

    /**
     * 修改app首页配置
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Catalogue catalogue) {
        catalogueService.updateById(catalogue);
        return SUCCESS_TIP;
    }

    /**
     * app首页配置详情
     */
    @RequestMapping(value = "/detail/{catalogueId}")
    @ResponseBody
    public Object detail(@PathVariable("catalogueId") Integer catalogueId) {
        return catalogueService.selectById(catalogueId);
    }
}
