package com.stylefeng.guns.modular.taobao.controller;

import com.stylefeng.guns.core.base.controller.BaseController;
import com.stylefeng.guns.core.base.tips.ErrorTip;
import com.stylefeng.guns.core.enums.ResultEnum;
import com.stylefeng.guns.core.util.ExcelImportUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.modular.system.model.Detail;
import com.stylefeng.guns.modular.taobao.service.IDetailService;
import org.springframework.web.multipart.MultipartFile;


/**
 * 淘宝券详情控制器
 *
 * @author fengshuonan
 * @Date 2018-06-13 22:53:06
 */
@Controller
@RequestMapping("/detail")
public class DetailController extends BaseController {

    private String PREFIX = "/taobao/detail/";

    @Autowired
    private IDetailService detailService;

    @Value("${cms.file.upload-file-path}")
    String fileFolder;
    /**
     * 跳转到淘宝券详情首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "detail.html";
    }

    /**
     * 跳转到添加淘宝券详情
     */
    @RequestMapping("/detail_add")
    public String detailAdd() {
        return PREFIX + "detail_add.html";
    }

    /**
     * 跳转到修改淘宝券详情
     */
    @RequestMapping("/detail_update/{detailId}")
    public String detailUpdate(@PathVariable Integer detailId, Model model) {
        Detail detail = detailService.selectById(detailId);
        model.addAttribute("item", detail);
        LogObjectHolder.me().set(detail);
        return PREFIX + "detail_edit.html";
    }

    /**
     * 跳转到批量上传详情
     */
    @RequestMapping("/detail_upload")
    public String detailUpload() {
        return PREFIX + "detail_upload.html";
    }

    /**
     * 获取淘宝券详情列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        return detailService.selectList(null);
    }

    /**
     * 新增淘宝券详情
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(Detail detail) {
        detailService.insert(detail);
        return SUCCESS_TIP;
    }

    /**
     * 删除淘宝券详情
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Integer detailId) {
        detailService.deleteById(detailId);
        return SUCCESS_TIP;
    }

    /**
     * 修改淘宝券详情
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(Detail detail) {
        detailService.updateById(detail);
        return SUCCESS_TIP;
    }

    /**
     * 淘宝券详情详情
     */
    @RequestMapping(value = "/detail/{detailId}")
    @ResponseBody
    public Object detail(@PathVariable("detailId") Integer detailId) {
        return detailService.selectById(detailId);
    }


    @ApiOperation(value="通过Excel上传文件", notes="根据文件写入到数据库")
    @ApiImplicitParam(name = "file", value = "入库文件", required = true, dataType = "MultipartFile")
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    @ResponseBody
    public Object uploadFile(MultipartFile file) {
        //判断文件是否为空
        if (null == file ) {
            return new ErrorTip(ResultEnum.FILE_EMPTY_ERROR.getCode(),
                    ResultEnum.FILE_EMPTY_ERROR.getMessage());
        }
        //获取文件名
        String fileName = file.getOriginalFilename();
        System.out.println(fileName);
        //验证文件名是否合格
        if (!ExcelImportUtils.validateExcel(fileName)) {
            return new ErrorTip(ResultEnum.FILE_TYPE_ERROR.getCode(),
                    ResultEnum.FILE_TYPE_ERROR.getMessage());
        }
        //进一步判断文件内容是否为空（即判断其大小是否为0或其名称是否为null)
        long size = file.getSize();
        if (size == 0){
            return new ErrorTip(ResultEnum.FILE_SIZE_EMPTY_ERROR.getCode(),
                    ResultEnum.FILE_SIZE_EMPTY_ERROR.getMessage());
        }

        String filepath = null;
        try {
            filepath = ClassUtils.getDefaultClassLoader().getResource("").getPath();
//            filepath = ResourceUtils.getURL("classpath:").getPath();
            System.out.println("当前项目路径------"+ filepath);

        } catch (Exception e) {
            e.printStackTrace();
        }
        //批量导入
        Boolean result = detailService.batchImport(filepath,fileName,file);
        if (!result) {
            return new ErrorTip(ResultEnum.FILE_SAVE_ERROR.getCode(),
                    ResultEnum.FILE_SAVE_ERROR.getMessage());
        }
        return SUCCESS_TIP;
    }
}
